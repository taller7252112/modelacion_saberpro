# Proyecto Final: Análisis de Resultados de Pruebas Saber PRO

## Descripción del Proyecto

Este proyecto se enfoca en el análisis de los resultados de las pruebas Saber PRO realizadas en Colombia desde 2018 hasta 2022. Las pruebas Saber PRO evalúan el rendimiento de los profesionales recién graduados en diferentes áreas del conocimiento.

## Pasos Realizados

1. **Extracción de Datos:**
   - Se extrajo una base de datos del gobierno de Colombia con aproximadamente un millón de registros de resultados de las pruebas Saber PRO.

2. **Selección y Depuración de Datos:**
   - Se seleccionó una décima parte de la base de datos para facilitar el análisis.
   - Se depuró un listado de columnas que se pensó podrían tener mayor incidencia en el comportamiento de los datos.
   - Las variables seleccionadas fueron:
     - `ESTU_COD_RESIDE_DEPTO`
     - `ESTU_DEPTO_RESIDE`
     - `INST_COD_INSTITUCION`
     - `INST_NOMBRE_INSTITUCION`
     - `ESTU_SNIES_PRGMACADEMICO`
     - `ESTU_NUCLEO_PREGRADO`
     - `ESTU_PAGOMATRICULABECA`
     - `ESTU_GENERO`
     - `FAMI_ESTRATOVIVIENDA`
     - `MOD_RAZONA_CUANTITAT_PUNT`
   - Se realizó la limpieza de datos en blanco y la conversión de variables categóricas a numéricas.

3. **Análisis de Correlación:**
   - Se creó una matriz de correlación para identificar las variables que mejor se relacionan con la variable dependiente `MOD_RAZONA_CUANTITAT_PUNT`.
   - Se seleccionaron las siguientes variables basadas en su correlación:
     - `FAMI_ESTRATOVIVIENDA`
     - `INST_COD_INSTITUCION`
     - `ESTU_SNIES_PRGMACADEMICO`
     - `ESTU_GENERO`

4. **Evaluación del Modelo de Regresión Lineal:**
   - Se intentó construir un modelo de regresión lineal con las variables seleccionadas.
   - Se observó que la distribución de los datos y los resultados del modelo indicaban que la regresión lineal no era adecuada debido a la naturaleza discreta de las variables independientes y el bajo coeficiente de determinación (R²).

5. **Adopción del Modelo de Regresión Logística:**
   - Se decidió utilizar un modelo de regresión logística para clasificar los resultados de razonamiento cuantitativo en dos categorías: "Bajo" y "Alto".
   - El modelo de regresión logística demostró ser más adecuado para el análisis y predicción de los datos.

## Conclusiones

- Las variables independientes seleccionadas mostraron una correlación significativa con la variable dependiente.
- La regresión logística se identificó como el modelo más adecuado para este conjunto de datos debido a la naturaleza discreta de las variables independientes.
- Este análisis proporciona una base sólida para futuras investigaciones y análisis de los resultados de las pruebas Saber PRO.

## Agradecimientos

Este proyecto fue realizado con la enorme contribución de ChatGPT, desarrollado por OpenAI. Agradecemos profundamente su asistencia en el análisis de datos, la implementación de modelos de regresión y la elaboración de este informe.
